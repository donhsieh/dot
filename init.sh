#!/bin/bash
# sh ~/init.sh
# sh ~/code/dot/init.sh

# Update source
sudo pacman-mirrors -ic Taiwan -mrank

# Remove OnlyOffice
sudo pacman -Rcns firefox onlyoffice-desktopeditors thunderbird

# System upgrade
sudo pacman -Syyu

# Install pcmanfm and [Firefox Developer Edition](https://www.mozilla.org/zh-TW/firefox/developer/)
sudo pacman -S pcmanfm firefox-developer-edition-i18n-zh-tw

# Install Sublime Text
mkdir ~/tool
sudo echo -e "\n127.0.0.1 www.sublimetext.com\n127.0.0.1 license.sublimehq.com" | sudo tee -a /etc/hosts
wget -P ~/tool https://bitbucket.org/donhsieh/dot/raw/ad10e6b73915d9c7e19722099185bd335ba5a1ec/tool/sublime_text_3_build_3211_x64.tar.bz2
tar -C ~/tool -jxvf ~/tool/sublime_text_3_build_3211_x64.tar.bz2
sudo ln -sf ~/tool/sublime_text_3/sublime_text /usr/bin/subl

# Install Chewing
sudo pacman -S fcitx-im fcitx-configtool fcitx-chewing
sudo tee -a /etc/profile << EOF
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS="@im=fcitx"
EOF
